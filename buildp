#!/bin/bash

# buildp - one more Debian package builder
# Copyright (C) 2021  Dmitry Mikhirev
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e -u

ARCH=$(dpkg-architecture -q DEB_HOST_ARCH)
CODENAME=$(lsb_release -c -s)

CACHE_DIR=/var/cache/buildp
RESULT_DIR=$CACHE_DIR/result
TEMP_DIR=/tmp

LOG_LEVEL=3

FORCE_UPDATE=
RUN_SHELL=

MKSQUASHFS_OPTS='-comp zstd'

for rcfile in /etc/buildprc "$HOME/.buildprc"; do
    test -r "$rcfile" && . "$rcfile"
done

while getopts a:c:o:usdv f; do
    case $f in
    a) ARCH=$OPTARG;;
    c) CODENAME=$OPTARG;;
    o) RESULT_DIR=$OPTARG;;
    u) FORCE_UPDATE=y;;
    s) RUN_SHELL=y;;
    d)
        LOG_LEVEL=4
        VERBOSE=y
        ;;
    v) VERBOSE=y;;
    \?)
        echo "unknown option '$f'" >&2
        exit 1
        ;;
    esac
done

shift $((OPTIND - 1))

IMAGE=${CACHE_DIR}/${CODENAME}_${ARCH}.sqfs

usage() {
    cat <<-EOF
	Usage: $0 [ -a <architecture> ] [ -c <codename> ] [ -d | -v ]
	This command must be run in a debianized source directory.
	Options:
	    -a <architecture> 
	        Build package for specified architecture instead host system.
	    -c <codename>
	        Build package for distribuition with specified codename instead host system.
	    -o <directory>
	        Copy output files to specified directory (it must exist).
	    -u
	        Update cached image if exists.
	    -s
	        Run interactive shell in the build environment after build process finished.
	    -v
	        Verbose output.
	    -d
	        Debug output.
	EOF
}

log() {
    local level=3

    OPTIND=1
    while getopts fewidt l; do
        case $l in
        f) level=0;; # fatal
        e) level=1;; # error
        w) level=2;; # warning
        i) level=3;; # info (default)
        d) level=4;; # debug
        t) level=5;; # trace
        \?) return 1;;
        esac
    done

    shift $((OPTIND - 1))

    if [ $level -le "$LOG_LEVEL" ]; then
        case $level in
        0) echo -n '[fatal] ';;
        1) echo -n '[error] ';;
        2) echo -n '[warning] ';;
        3) echo -n '[info] ';;
        4) echo -n '[debug] ';;
        5) echo -n '[trace] ';;
        esac

        echo "$*"
    fi

    if [ "$level" = 0 ]; then
        exit 1
    fi
}

create_image() {
    log -t "create_image $*"

    local cachedir=${CACHE_DIR}/debootstrap/${CODENAME}
    mkdir -p "$cachedir"

    local tmpdir=$(mktemp -p "$TEMP_DIR" -d buildp.image.XXXXXXXX)
    TEMPFILES=("$tmpdir" "${TEMPFILES[@]}")
    log -d "created temporary directory $tmpdir"

    log -i 'running debootstrap...'
    debootstrap \
        --arch="$ARCH" \
        --variant=buildd \
        --include=fakeroot \
        --cache-dir="$cachedir" \
        ${VERBOSE:+--verbose} \
        "$CODENAME" "$tmpdir"

    useradd -R "$tmpdir" -m builder

    local tmp_image=$(mktemp "$IMAGE.XXXXXXXX")
    TEMPFILES=("$tmp_image" "${TEMPFILES[@]}")
    mksquashfs "$tmpdir"/* "$tmp_image" -noappend $MKSQUASHFS_OPTS

    local old_users="$IMAGE-$( stat -c %Y "$IMAGE" ).users"

    flock ${VERBOSE:+--verbose} "$IMAGE" mv ${VERBOSE:+-v} -f "$tmp_image" "$IMAGE"

    if [ -e "$old_users" -a \! -s "$old_users" ]; then
        rm ${VERBOSE:+-v} -f "$old_users"
    fi

    rm ${VERBOSE:+-v} -rf "$tmpdir"
}

push_mount() {
    mount ${VERBOSE:+-v} "$@"
    eval dir=\$$#
    MOUNTPOINTS=("$dir" "${MOUNTPOINTS[@]}")
}

prepare_chroot() {
    log -t "prepare_chroot $*"

    if [ \! -e "$IMAGE" -o -n "$FORCE_UPDATE" ]; then
        local starttime=$( date  +%s )
        {
            log -d "trying to lock $IMAGE.create"
            if flock ${VERBOSE:+--verbose} -n 33; then
                log -i "building image $IMAGE"
                create_image
            else
                log -i "waiting while another process is (re)building $IMAGE"
                log -d "locking $IMAGE.create"
                flock ${VERBOSE:+--verbose} 33
                if [ \! -e "$IMAGE" -o "$( stat -c %Y "$IMAGE" )" -lt "$starttime" ]; then
                    log -i "image has not updated, rebuilding"
                    create_image
                fi
            fi
        } 33>"$IMAGE.create"
    else
        log -i "using cached image $IMAGE"
    fi

    local basedir=$1

    mkdir "$basedir/upper" "$basedir/work" "$basedir/root"

    {
        log -d "locking $IMAGE"
        flock ${VERBOSE:+--verbose} 34

        local ts=$(stat -c %Y "$IMAGE")
        LOWER=$IMAGE-$ts

        {
            log -d "locking $LOWER.users"
            flock ${VERBOSE:+--verbose} 35

            local curusers
            if [ $(stat -c %Y "$LOWER.users") > $(stat -c %Y /proc/1) ]; then
                curusers=$( while read pid; do
                    if [ -n "$pid" -a -d "/proc/$pid" ]; then echo "$pid"; fi
                done <&35 )
                log -d "current image users:
$curusers"
            else
                log -d "file $LOWER.users last modified before reboot, discarding its contents"
            fi

            cat > "$LOWER.users" <<-EOF
		$curusers
		$$
		EOF
        } 35<> "$LOWER.users"

        log -d "released lock on $LOWER.users"

        if [ -z "$curusers" ]; then
            mkdir -p "$LOWER"
            mount ${VERBOSE:+-v} -t squashfs "$IMAGE" "$LOWER"
        fi

    } 34< "$IMAGE"

    log -d "released lock on $IMAGE"

    push_mount -t overlay overlay \
        -olowerdir="$LOWER",upperdir="$basedir/upper",workdir="$basedir/work" \
        "$basedir/root"

    for b in proc sys dev; do
        push_mount --bind "/$b" "$basedir/root/$b"
    done
}

cleanup() {
    # try to finish cleanup even if error occured
    set +e +u

    log -t "cleanup $*"

    if [ ${#MOUNTPOINTS[@]} -gt 0 ]; then
        umount ${VERBOSE:+-v} "${MOUNTPOINTS[@]}"
    fi

    if [ ${#TEMPFILES[@]} -gt 0 ]; then
        rm ${VERBOSE:+-v} -rf "${TEMPFILES[@]}"
    fi

    if [ -n "${LOWER-}" ]; then
        {
            log -d "locking $LOWER.users"

            flock ${VERBOSE:+--verbose} 36

            local curusers=$( while read pid; do
                if [ -n "$pid" -a "$pid" != $$ -a -d "/proc/$pid" ]; then echo "$pid"; fi
            done <&36 )

            if [ -n "$curusers" ]; then
                log -d "current image users:
$curusers"
                echo "$curusers" > "$LOWER.users"
            else
                log -d 'nobody is using the image, unmounting'
                umount ${VERBOSE:+-v} "$LOWER" && rmdir ${VERBOSE:+-v} "$LOWER"
                if [ "$IMAGE-$( stat  -c %Y "$IMAGE" )" = "$LOWER" ]; then
                    :> "$LOWER.users"
                else
                    log -d "file $LOWER.users is obsolete, removing"
                    rm ${VERBOSE:+-v} -f "$LOWER.users"
                fi
            fi
        } 36<> "$LOWER.users"

        log -d "released lock on $LOWER.users"
    fi
}

copy_dsc() {
    log -t "copy_dsc $*"

    local dsc=$1
    local target=$2

    local files=$( awk '
        BEGIN       {p=0}
        ! /^[ \t]/  {p=0}
        /^[ \t]*$/  {p=0}
        /^Files:/   {p=1}
        /^ / && p   {print $3}
    ' <"$dsc" )

    local dir=$(dirname "$dsc")
    for f in $files; do
        cp ${VERBOSE:+-v} "$dir/$f" "$target/"
    done

    cp ${VERBOSE:+-v} -- "$dsc" "$target/"
}

copy_changes() {
    log -t "copy_changes $*"

    local changes=$1
    local target=$2

    local files=$( awk '
        BEGIN       {p=0}
        ! /^[ \t]/  {p=0}
        /^[ \t]*$/  {p=0}
        /^Files:/   {p=1}
        /^ / && p   {print $5}
    ' <"$changes" )

    local dir=$(dirname "$changes")
    for f in $files; do
        cp ${VERBOSE:+-v} "$dir/$f" "$target/"
    done

    cp ${VERBOSE:+-v} -- "$changes" "$target/"
}

run_as() {
    log -t "run_as $*"

    local user=$1
    local group=$2
    shift 2
    local cmd=$*

    su -s /bin/sh -c "$cmd" -g "$group" "$user"
}

MOUNTPOINTS=()
TEMPFILES=()

trap cleanup EXIT
trap '' PIPE

if [ ! -f debian/control -o ! -f debian/changelog ]; then
    usage
    exit 1
fi

if [ $(id -u) -ne 0 ]; then
    log -f 'this program must be run by root user'
fi

PKG_SOURCENAME=$(dpkg-parsechangelog | sed -n 's/^Source: //p')
PKG_VERSION=$(dpkg-parsechangelog | sed -n 's/^Version: \(.*:\|\)//p')
PKG_SOURCEVERSION=$(echo "$PKG_VERSION" | sed 's/-.*//')

# save intermediate files with the same owner and group as CWD
REAL_USER=$(stat -c %U .)
REAL_GROUP=$(stat -c %G .)

BASE_DIR=$(mktemp -p "$TEMP_DIR" -d buildp.build.XXXXXXXX)
TEMPFILES=("$BASE_DIR" "${TEMPFILES[@]}")
log -d "created temporary working directory $BASE_DIR"

prepare_chroot "$BASE_DIR"

run_as "$REAL_USER" "$REAL_GROUP" dpkg-source --build .
DSC=../${PKG_SOURCENAME}_${PKG_VERSION}.dsc

copy_dsc "$DSC" "$BASE_DIR/root/home/builder/"
chroot "$BASE_DIR/root" chown -R builder /home/builder

chroot "$BASE_DIR/root" \
    env LC_ALL=C \
    su -c "
        set -e
        cd /home/builder
        dpkg-source -x '$(basename "$DSC")'
        cd '$PKG_SOURCENAME-$PKG_SOURCEVERSION'
    " builder

log -i 'instaling build dependencies...'
chroot "$BASE_DIR/root" \
    env LC_ALL=C \
    apt-get -y build-dep --no-install-recommends "/home/builder/$(basename "$DSC")"

log -i 'building package...'
if chroot "$BASE_DIR/root" \
    env LC_ALL=C \
    su -c "
        set -e
        cd '/home/builder/$PKG_SOURCENAME-$PKG_SOURCEVERSION'
        dpkg-buildpackage -us -uc
    " builder
then
    copy_changes \
        "$BASE_DIR/root/home/builder/${PKG_SOURCENAME}_${PKG_VERSION}_${ARCH}.changes" \
        "$RESULT_DIR"

    log -i "results copied to $RESULT_DIR"
else
    log -e 'build failed'
fi

if [ -n "$RUN_SHELL" ]; then
    chroot "$BASE_DIR/root" \
        env LC_ALL=C \
        su -l builder
fi
